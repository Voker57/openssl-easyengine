#pragma once

#include <openssl/obj_mac.h>

#include <stdint.h> /* uint8_t */
#include <stddef.h> /* size_t */

typedef struct {
	uint8_t *pubk;
	uint8_t *privk;
	char has_private;
} EASYENGINE_KEYPAIR;

typedef enum {
	NO_FLAG=0,
	NO_PRIV_KEY=1,
} easyengine_keypair_flags_t;

EASYENGINE_KEYPAIR *easyengine_keypair_new(easyengine_keypair_flags_t flags);

int easyengine_keypair_free(EASYENGINE_KEYPAIR *keypair);

#define _easyengine_keypair_is_invalid(kp, contains_private) \
	( (kp) == NULL || ( (contains_private) && (1 != (kp)->has_private) ))

#define easyengine_keypair_is_invalid(kp) \
	_easyengine_keypair_is_invalid((kp), 0)
#define easyengine_keypair_is_invalid_private(kp) \
	_easyengine_keypair_is_invalid((kp), 1)

// cipher name, in machine friendly form, i.e. "ZEA-256"
char *alg_name;
// cipher oid
char *alg_oid;

// size of secret key
unsigned int pkey_sk_size;
// size of private key
unsigned int pkey_pk_size;
// overhead of signed data over plaintext
unsigned int pkey_signature_overhead;


// functions return 1 if successful unless stated otherwise

// generate a keypair
int pkey_generate_keypair(unsigned char* pk, unsigned char* sk);
// sign a message
// set smlen to a signed message length, max = mlen + pkey_overhead
int pkey_sign(unsigned char *sm, unsigned long long *smlen, const unsigned char *m, unsigned long long mlen, const unsigned char *sk);
// verify a message
// set smlen to an original message length, max = smlen
int pkey_verify(unsigned char *m, unsigned long long *mlen const unsigned char *sm, unsigned long long smlen, const unsigned char *pk);

// overhead of encrypted data over plaintext
unsigned int pkey_encryption_overhead;

// decrypt a message
// set clen to an encrypted message length, max = mlen +
int pkey_encrypt(unsigned char *c, unsigned long long *clen, const unsigned char* m, unsigned long long mlen, const unsigned char *pk);
int pkey_decrypt(unsigned char *m, unsigned long long *mlen, const unsigned char* c, unsigned long long clen, const unsigned char *sk);

// overhead of encrypted data over plaintext
unsigned int kem_overhead;
// size of key being encapsulated
unsigned int kem_secret_size;
// turn given data into key and ct from which the key can be restored
int kem_encapsulate(unsigned char *ct, unsigned char *ss, const unsigned char *pk);
// turn given ciphertext back into key
int kem_decapsulate(unsigned char *ss, const unsigned char *ct, const unsigned char *sk);

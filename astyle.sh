#!/bin/sh
find \( -iname '*.c' -or -iname '*.h' \) -exec astyle --style=java --indent=force-tab --indent-after-parens {} \;

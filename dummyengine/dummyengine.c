#include <string.h>
#include <stdio.h>

static char *padding = "JJJJJJJJJ";

static void hexx(const char *hint,  const unsigned char* dat, const int len) {
	printf("%s: ", hint);
	for(int i =0; i < len; i++) {
		printf("%0x", dat[i]);
	}
	printf("\n");
}

// engine id, string
char *engine_id = "dummycrypt";
// algorithm name, in machine friendly form, i.e. "ZEA-256"
char *alg_name = "DUMMY";
// algorithm OID
char *alg_oid = "2.25.145179077385568906198441193185243749890.1";


// size of public key
size_t pkey_pk_size = 10;
// size of private key
size_t pkey_sk_size = 16;

// generate a keypair
int pkey_generate_keypair(unsigned char* pk, unsigned char* sk) {
	memcpy(pk, "PUBLICKEY", pkey_pk_size);
	memcpy(sk, "THISISSECRETKEY", pkey_sk_size);
}
// }}}

// functions return 1 if successful unless stated otherwise

// Define this group if algorithm implements signatures {{{

size_t pkey_signature_size(size_t mlen) {
	return 10;
}

// sign a message
// set smlen to a signed message length, max = mlen + pkey_overhead
int pkey_sign(unsigned char *sig, size_t *siglen, const unsigned char *m, size_t mlen, const unsigned char *sk) {

	int additional_space = m[0] % 10;

	*siglen = additional_space;
	memcpy(sig, padding, *siglen);

	return
		memcmp(sk, "THISISSECRETKEY", pkey_sk_size) == 0;
}
// verify a message
// set smlen to an original message length, max = smlen
int pkey_verify(unsigned char *sig, size_t siglen, const unsigned char *m, size_t mlen, const unsigned char *pk) {
	int additional_space = m[0] % 10;
	return
		siglen == additional_space
		&& memcmp(sig, padding, additional_space) == 0
		&& memcmp(pk, "PUBLICKEY", pkey_pk_size) == 0;
}
// }}}



// Define this group if algoritm implements asymmetric encryption {{{

// overhead of encrypted data over plaintext
size_t pkey_encryption_overhead = 10;

// decrypt a message
// set clen to an encrypted message length, max = mlen +
int pkey_encrypt(unsigned char *c, size_t *clen, const unsigned char* m, size_t mlen, const unsigned char *pk) {
	memmove(c, m, mlen);
	// ROT13
	// Some deterministic overhead to test its handling

	int additional_space = c[0] % 10;

	*clen = mlen + additional_space;
	memcpy(c + mlen, padding, additional_space);

	for(int i=0; i < *clen; i++) {
		c[i] += 13;
	}

	return memcmp(pk, "PUBLICKEY", pkey_pk_size) == 0;
}

int pkey_decrypt(unsigned char *m, size_t *mlen, const unsigned char* c, size_t clen, const unsigned char *sk) {
	memmove(m, c, clen);

	// ROT13
	for(int i=0; i < *mlen; i++) {
		m[i] -= 13;
	}

	int additional_space = m[0] % 10;

	*mlen=clen-additional_space;

	return memcmp(m+*mlen, padding, additional_space) == 0 && memcmp(sk, "THISISSECRETKEY", pkey_sk_size) == 0;
}

// }}}


// Define this group if algorithm implements KEM {{{

// overhead of encrypted data over plaintext
size_t kem_encrypted_key_size = 43;
// size of key being encapsulated
size_t kem_secret_size = 32;
// turn given data into key and ct from which the key can be restored


int kem_encapsulate(unsigned char *ct, unsigned char *ss, const unsigned char *pk) {
	unsigned char first;

	if(memcmp(pk, "PUBLICKEY", pkey_pk_size) != 0) {
		return 0;
	}
	// shift ss right by 1 byte
	first = ss[0];
	memmove(ss, ss+1, kem_secret_size-1);
	ss[kem_secret_size-1] = first;
	memcpy(ct, ss, kem_secret_size);

	// shift ct by 1 more byte
	first = ct[0];
	memmove(ct, ct+1, kem_secret_size-1);
	ct[kem_secret_size-1] = first;

	// append a text to a key
	memcpy(ct + kem_secret_size, "<-THISISIT", 11);

	return 1;
}
// turn given ciphertext back into key
int kem_decapsulate(unsigned char *ss, const unsigned char *ct, const unsigned char *sk) {
	if(memcmp(sk, "THISISSECRETKEY", pkey_pk_size) != 0) {
		return 0;
	}

	// shift 32 bytes left by 1 byte
	memcpy(ss+1, ct, kem_secret_size - 1);
	ss[0] = ct[kem_secret_size-1];

	// assert the rest is right
	if(memcmp(ct+kem_secret_size, "<-THISISIT", 11) == 0) {
		return 1;
	} else {
		return 0;
	}
}

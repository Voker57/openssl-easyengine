#include <string.h>
#include <stdio.h>

static char *padding = "JJJJJJJJJ";

// engine id, string
char *engine_id = "encengine";
// algorithm name, in machine friendly form, i.e. "ZEA-256"
char *alg_name = "DUMMYENC";
// algorithm OID
char *alg_oid = "2.25.87790022612325164897946209370368738784";

// size of secret key
size_t pkey_pk_size = 10;
// size of private key
size_t pkey_sk_size = 16;

// generate a keypair
int pkey_generate_keypair(unsigned char* pk, unsigned char* sk) {
	memcpy(pk, "PUBLICKEY", pkey_pk_size);
	memcpy(sk, "THISISSECRETKEY", pkey_sk_size);
}

// Define this group if algoritm implements asymmetric encryption {{{

// overhead of encrypted data over plaintext
size_t pkey_encryption_overhead = 10;

// decrypt a message
// set clen to an encrypted message length, max = mlen +
int pkey_encrypt(unsigned char *c, size_t *clen, const unsigned char* m, size_t mlen, const unsigned char *pk) {
	memmove(c, m, mlen);
	// ROT13
	// Some deterministic overhead to test its handling

	int additional_space = c[0] % 10;

	*clen = mlen + additional_space;
	memcpy(c + mlen, padding, additional_space);

	for(int i=0; i < *clen; i++) {
		c[i] += 13;
	}

	return memcmp(pk, "PUBLICKEY", pkey_pk_size) == 0;
}

int pkey_decrypt(unsigned char *m, size_t *mlen, const unsigned char* c, size_t clen, const unsigned char *sk) {
	memmove(m, c, clen);

	// ROT13
	for(int i=0; i < *mlen; i++) {
		m[i] -= 13;
	}

	int additional_space = m[0] % 10;

	*mlen=clen-additional_space;

	return memcmp(m+*mlen, padding, additional_space) == 0 && memcmp(sk, "THISISSECRETKEY", pkey_sk_size) == 0;
}

// }}}

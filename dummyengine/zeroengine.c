#include <string.h>
#include <stdio.h>
// engine id, string
char *engine_id = "zerocrypt";
// algorithm name, in machine friendly form, i.e. "ZEA-256"
char *alg_name = "ZERO";
// algorithm OID
char *alg_oid = "2.25.198035725451982707956409453482919695565";

// size of secret key
size_t pkey_pk_size = 10;
// size of private key
size_t pkey_sk_size = 16;

// generate a keypair
int pkey_generate_keypair(unsigned char* pk, unsigned char* sk) {
	memcpy(pk, "PUBLICKEY", pkey_pk_size);
	memcpy(sk, "THISISSECRETKEY", pkey_sk_size);
}

#!/bin/bash

test_description="Load dummy encryption engine and test asymmetric crypto with it"
export ENGINE_PATH="$PWD/../build/easyengine.so"
export EASYENGINE_ENGINE="$PWD/../build/libencengine.so"
export ENGINE_NAME="encengine"
ALG="DUMMYENC"
ASSERT_NOERROR="2>&1 | tee err && ! grep -Pq '^\d+:error:' err"
OPENSSL=${OPENSSL:-$(which openssl)}
WOPENSSL="OPENSSL_CONF='$PWD/encengine.cnf' $OPENSSL"

. ./lib/sharness/sharness.sh

# Macro to redirect output to file and scan it for errors
# Because OpenSSL would still return 0 if failed to load engine

set -o pipefail

test_expect_success "Load engine" "
	$WOPENSSL engine -c $ENGINE_NAME $ASSERT_NOERROR
"

test_expect_success "Generate keypair" "
	$WOPENSSL genpkey -algorithm $ALG -out keypair.pem $ASSERT_NOERROR
"

# pkeyutl does not work with data > EVP_PKEY_size() * 10
test_expect_success "Encrypt a message" "
	dd if=/dev/urandom of=message.dat count=1 bs=10 && 
	$WOPENSSL pkeyutl -encrypt -inkey keypair.pem -in message.dat -out message.crypted $ASSERT_NOERROR
"

test_expect_success "Decrypt a message" "
	$WOPENSSL pkeyutl -decrypt -inkey keypair.pem -in message.crypted -out message.decrypted $ASSERT_NOERROR
"

test_done

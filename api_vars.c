#include "api_vars.h"

/* This file defines an easy engine api
 * To implement a new engine, simply provide symbols below in your shared library
 * minus the *
 * Example in dummyengine/dummyengine.c
 */

// Mandatory definitions {{{


// engine id, string
char **engine_id;
// algorithm name, in machine friendly form, i.e. "ZEA-256"
char **alg_name;
// algorithm OID
char **alg_oid;


// size of secret key
size_t *pkey_sk_size;
// size of public key
size_t *pkey_pk_size;

// generate a keypair
int (*pkey_generate_keypair)(unsigned char* pk, unsigned char* sk);

// }}}


// Define this group if algorithm implements signatures {{{

// functions return 1 if successful unless stated otherwise

// return maximum signature size for given text size
size_t (*pkey_signature_size)(size_t mlen);
// sign a message
int (*pkey_sign)(unsigned char *sig, size_t *siglen, const unsigned char *m, size_t mlen, const unsigned char *sk);
// verify a message
int (*pkey_verify)(const unsigned char *sig, size_t siglen, const unsigned char *m, size_t mlen, const unsigned char *pk);

// }}}


// Define this group if algoritm implements asymmetric encryption {{{

// overhead of encrypted data over plaintext
size_t *pkey_encryption_overhead;

// decrypt a message
// set clen to an encrypted message length, max = mlen +
int (*pkey_encrypt)(unsigned char *c, size_t *clen, const unsigned char* m, size_t mlen, const unsigned char *pk);
int (*pkey_decrypt)(unsigned char *m, size_t *mlen, const unsigned char* c, size_t clen, const unsigned char *sk);

// }}}

// Define this group if algorithm implements KEM {{{

// size of encrypted key
size_t *kem_encrypted_key_size;
// size of key being encapsulated
size_t *kem_secret_size;
// turn given data into key and ct from which the key can be restored
int (*kem_encapsulate)(unsigned char *ct, unsigned char *ss, const unsigned char *pk);
// turn given ciphertext back into key
int (*kem_decapsulate)(unsigned char *ss, const unsigned char *ct, const unsigned char *sk);

// }}}

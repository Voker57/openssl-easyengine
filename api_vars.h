#pragma once

#include <stddef.h>

// See api_vars.c for documentation and modify both files at the same time

extern char **engine_id;
extern char **alg_name;
extern char **alg_oid;

extern size_t *pkey_sk_size;
extern size_t *pkey_pk_size;
extern int (*pkey_generate_keypair)(unsigned char* pk, unsigned char* sk);

extern size_t (*pkey_signature_size)(size_t mlen);
extern int (*pkey_sign)(unsigned char *sig, size_t *siglen, const unsigned char *m, size_t mlen, const unsigned char *sk);
extern int (*pkey_verify)(const unsigned char *sig, size_t siglen, const unsigned char *m, size_t mlen, const unsigned char *pk);

extern size_t *pkey_encryption_overhead;

extern int (*pkey_encrypt)(unsigned char *c, size_t *clen, const unsigned char* m, size_t mlen, const unsigned char *pk);
extern int (*pkey_decrypt)(unsigned char *m, size_t *mlen, const unsigned char* c, size_t clen, const unsigned char *sk);

extern size_t *kem_encrypted_key_size;
extern size_t *kem_secret_size;
extern int (*kem_encapsulate)(unsigned char *ct, unsigned char *ss, const unsigned char *pk);
extern int (*kem_decapsulate)(unsigned char *ss, const unsigned char *ct, const unsigned char *sk);

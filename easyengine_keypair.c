#include "easyengine_keypair.h"
#include "api_vars.h"
#include "debug/debug.h"

#include <openssl/crypto.h>
#include <openssl/ec.h>
#include <openssl/err.h>


EASYENGINE_KEYPAIR *easyengine_keypair_new(easyengine_keypair_flags_t flags) {
	EASYENGINE_KEYPAIR *kpair = NULL;

	kpair = OPENSSL_secure_malloc(sizeof(*kpair));
	if (kpair == NULL) {
		goto err;
	}

	kpair->has_private = 0;
	kpair->pubk = OPENSSL_secure_malloc(*pkey_pk_size);
	if(kpair->pubk == NULL) {
		errorf("malloc failed\n");
		goto err;
	}

	if (0 == (flags & NO_PRIV_KEY) ) {
		kpair->has_private = 1;
		kpair->privk = OPENSSL_secure_malloc(*pkey_sk_size);
		if(kpair->privk == NULL) {
			errorf("malloc failed\n");
			goto err;
			return 0;
		}

	}

	return kpair;
err:
	if (kpair) {
		if (kpair->pubk) {
			OPENSSL_secure_free(kpair->pubk);
		}

		if (kpair->privk) {
			OPENSSL_secure_free(kpair->privk);
		}

		OPENSSL_secure_free(kpair);
	}

	return NULL;
}

int easyengine_keypair_free(EASYENGINE_KEYPAIR *kpair) {
	if (!kpair)
		return 0;
	if (kpair->pubk) {
		OPENSSL_secure_free(kpair->pubk);
	}

	if (kpair->privk) {
		OPENSSL_secure_free(kpair->privk);
	}

	OPENSSL_secure_free(kpair);

	return 1;
}

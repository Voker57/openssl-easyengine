#include <openssl/err.h>
#include <openssl/x509.h>

#include <string.h>

#include "meths/easyengine_asn1_meth.h"
#include "api_vars.h"
#include "easyengine_keypair.h"
#include "debug/debug.h"
#include "easyengine.h"

#define EASYENGINE_EVP_PKEY_ASN1_FLAGS 0

typedef enum {
	EASYENGINE_PUBLIC,
	EASYENGINE_PRIVATE
} easyengine_key_op_t;

static int easyengine_key_print( BIO *bp, const EVP_PKEY *pkey,
	int indent, ASN1_PCTX *ctx, easyengine_key_op_t op) {
	if (!pkey)
		return 0;

	const EASYENGINE_KEYPAIR *kpair = EVP_PKEY_get0(pkey);

	if (op == EASYENGINE_PRIVATE) {
		if (easyengine_keypair_is_invalid_private(kpair)) {
			if (BIO_printf(bp, "%*s<INVALID PRIVATE KEY>\n", indent, "") <= 0)
				return 0;
			return 1;
		}
		if (BIO_printf(bp, "%*s%s Private-Key:\n", indent, "", *alg_name) <= 0)
			return 0;
		if (BIO_printf(bp, "%*spriv:\n", indent, "") <= 0)
			return 0;
		if (ASN1_buf_print(bp, kpair->privk,  *pkey_sk_size, indent + 4) == 0)
			return 0;
	} else {
		if (easyengine_keypair_is_invalid(kpair)) {
			if (BIO_printf(bp, "%*s<INVALID PUBLIC KEY>\n", indent, "") <= 0)
				return 0;
			return 1;
		}
		if (BIO_printf(bp, "%*s%s Public-Key:\n", indent, "", *alg_name) <= 0)
			return 0;
	}
	if (BIO_printf(bp, "%*spub:\n", indent, "") <= 0)
		return 0;
	if (ASN1_buf_print(bp, kpair->pubk, *pkey_pk_size,
			indent + 4) == 0)
		return 0;
	return 1;
}

static int easyengine_priv_print(BIO *bp, const EVP_PKEY *pkey, int indent, ASN1_PCTX *ctx) {
	return easyengine_key_print(bp, pkey, indent, ctx, EASYENGINE_PRIVATE);
}

static int easyengine_pub_print(BIO *bp, const EVP_PKEY *pkey, int indent, ASN1_PCTX *ctx) {
	return easyengine_key_print(bp, pkey, indent, ctx, EASYENGINE_PUBLIC);
}


static int easyengine_pub_cmp(const EVP_PKEY *a, const EVP_PKEY *b) {
	const EASYENGINE_KEYPAIR *akey = EVP_PKEY_get0(a);
	const EASYENGINE_KEYPAIR *bkey = EVP_PKEY_get0(b);


	if (easyengine_keypair_is_invalid(akey) || easyengine_keypair_is_invalid(bkey) )
		return -2;
	return !CRYPTO_memcmp(akey->pubk, bkey->pubk,
			*pkey_pk_size);
}

static void easyengine_free(EVP_PKEY *pkey) {
	EASYENGINE_KEYPAIR *kp = EVP_PKEY_get0(pkey);

	easyengine_keypair_free(kp);
}

/* TODO: "parameters" are always equal ? */
static int easyengine_cmp_parameters(const EVP_PKEY *a, const EVP_PKEY *b) {
	return 1;
}

static int easyengine_ctrl(EVP_PKEY *pkey, int op, long arg1, void *arg2) {
	EASYENGINE_KEYPAIR *kp = NULL;
	const unsigned char *p = NULL;
	int pklen = 0;

	switch (op) {
	case ASN1_PKEY_CTRL_SET1_TLS_ENCPT:
//         debug("nid: %d, op: ASN1_PKEY_CTRL_SET1_TLS_ENCPT, pklen: %ld\n", nid, arg1);
		p = arg2;
		pklen = arg1;

		if (p == NULL || pklen != *pkey_pk_size ) {
			// SUOLAerr(SUOLA_F_ASN1_GENERIC_CTRL, SUOLA_R_WRONG_LENGTH);
			return 0;
		}

		kp = easyengine_keypair_new(NO_PRIV_KEY);
		if (easyengine_keypair_is_invalid(kp)) {
			return 0;
		}

		memcpy(kp->pubk, p, pklen);

		EVP_PKEY_assign(pkey, NID_alg, kp);
		return 1;


	case ASN1_PKEY_CTRL_GET1_TLS_ENCPT:
//         debug("nid: %d, op: ASN1_PKEY_CTRL_GET1_TLS_ENCPT\n", nid);
		kp = EVP_PKEY_get0(pkey);
		if (!easyengine_keypair_is_invalid(kp)) {
			unsigned char **ppt = arg2;
			*ppt = OPENSSL_memdup(kp->pubk, *pkey_pk_size);
			if (*ppt != NULL)
				return *pkey_pk_size;
		}
		return 0;
	case ASN1_PKEY_CTRL_DEFAULT_MD_NID:
//         debug("nid: %d, op: ASN1_PKEY_CTRL_DEFAULT_MD_NID, ret: %s\n",
//                 nid, OBJ_nid2sn(nid_data->default_md_nid) );
		*(int *)arg2 = NID_sha512;
		return 2;

	default:
		return -2;

	}
}

static int easyengine_priv_encode(PKCS8_PRIV_KEY_INFO *p8, const EVP_PKEY *pkey) {
	const EASYENGINE_KEYPAIR *kp = EVP_PKEY_get0(pkey);
	ASN1_OCTET_STRING oct;
	unsigned char *penc = NULL;
	int penclen;
	char *tmp_buf = NULL;
	int ret = 0;

	if (easyengine_keypair_is_invalid(kp)) {
//         SUOLAerr(SUOLA_F_ASN1_GENERIC_PRIV_ENCODE, SUOLA_R_INVALID_PRIVATE_KEY);
		return 0;
	}

	tmp_buf = OPENSSL_secure_malloc(*pkey_sk_size+*pkey_pk_size);
	if (NULL == tmp_buf) {
//         SUOLAerr(SUOLA_F_ASN1_GENERIC_PRIV_ENCODE, ERR_R_MALLOC_FAILURE);
		return 0;
	}


	// TODO: write two octet strings instead?
	memcpy(tmp_buf, kp->privk, *pkey_sk_size);
	memcpy(tmp_buf+*pkey_sk_size, kp->pubk, *pkey_pk_size);
	oct.data = tmp_buf;
	oct.length = *pkey_sk_size+ *pkey_pk_size;
	oct.flags = 0;

	penclen = i2d_ASN1_OCTET_STRING(&oct, &penc);
	if (penclen < 0) {
//         SUOLAerr(SUOLA_F_ASN1_GENERIC_PRIV_ENCODE, ERR_R_MALLOC_FAILURE);
		ret = 0;
		goto err;
	}

	if (!PKCS8_pkey_set0(p8, OBJ_nid2obj(NID_alg), 0,
			V_ASN1_UNDEF, NULL, penc, penclen)) {
		OPENSSL_clear_free(penc, penclen);
//         SUOLAerr(SUOLA_F_ASN1_GENERIC_PRIV_ENCODE, ERR_R_MALLOC_FAILURE);
		ret = 0;
		goto err;
	}

	ret = 1;
err:
	if (tmp_buf)
		OPENSSL_secure_free(tmp_buf);
	return ret;
}

static int easyengine_priv_decode(EVP_PKEY *pkey, const PKCS8_PRIV_KEY_INFO *p8) {
	const unsigned char *p;
	int plen;
	ASN1_OCTET_STRING *oct = NULL;
	const X509_ALGOR *palg;
	EASYENGINE_KEYPAIR *kp = NULL;

	if (!PKCS8_pkey_get0(NULL, &p, &plen, &palg, p8)) {
		errorf("get0 failed\n");
		return 0;
	}

	oct = d2i_ASN1_OCTET_STRING(NULL, &p, plen);
	if (oct == NULL) {
		p = NULL;
		plen = 0;
	} else {
		p = ASN1_STRING_get0_data(oct);
		plen = ASN1_STRING_length(oct);
	}

	if (palg != NULL) {
		int ptype;

		/* Algorithm parameters must be absent */
		X509_ALGOR_get0(NULL, &ptype, NULL, palg);
		if (ptype != V_ASN1_UNDEF) {
			errorf("params failed\n");
			return 0;
		}
	}

	if (p == NULL || plen != *pkey_sk_size + *pkey_pk_size) {
		errorf("len failed: %d != %d \n", p, plen, *pkey_sk_size + *pkey_pk_size);
		return 0;
	}

	kp = easyengine_keypair_new(NO_FLAG);
	if (easyengine_keypair_is_invalid_private(kp)) {
		errorf("valid failed\n");
		return 0;
	}



	memcpy(kp->privk, p, *pkey_sk_size);
	memcpy(kp->pubk, p + *pkey_sk_size, *pkey_pk_size);

	ASN1_OCTET_STRING_free(oct);
	oct = NULL;
	p = NULL;
	plen = 0;

	EVP_PKEY_assign(pkey, NID_alg, kp);

	return 1;
}

static int easyengine_pub_encode(X509_PUBKEY *pk, const EVP_PKEY *pkey) {
	const EASYENGINE_KEYPAIR *kp = EVP_PKEY_get0(pkey);
	unsigned char *penc;

	if (easyengine_keypair_is_invalid(kp)) {
		errorf("Keypair is invalid\n");
		return 0;
	}

	penc = OPENSSL_memdup(kp->pubk, *pkey_pk_size);
	if (penc == NULL) {
		errorf("memdup failed\n");
		return 0;
	}

	if (!X509_PUBKEY_set0_param(pk, OBJ_nid2obj(NID_alg), V_ASN1_UNDEF,
			NULL, penc, *pkey_pk_size)) {
		OPENSSL_free(penc);
		errorf("malloc failed\n");
		return 0;
	}
	return 1;
}

static int easyengine_pub_decode(EVP_PKEY *pkey, X509_PUBKEY *pubkey) {
	const unsigned char *p;
	int pklen;
	X509_ALGOR *palg;
	EASYENGINE_KEYPAIR *kp = NULL;

	if (!X509_PUBKEY_get0_param(NULL, &p, &pklen, &palg, pubkey))
		return 0;

	if (palg != NULL) {
		int ptype;

		/* Algorithm parameters must be absent */
		X509_ALGOR_get0(NULL, &ptype, NULL, palg);
		if (ptype != V_ASN1_UNDEF) {
			errorf("Bogus parameters present\n");
			return 0;
		}
	}

	if (p == NULL || pklen != *pkey_pk_size) {
		errorf("Bad pubk size\n");
		return 0;
	}

	kp = easyengine_keypair_new(NO_PRIV_KEY);
	if ( easyengine_keypair_is_invalid(kp) ) {
		errorf("Keypair is invalid");
		return 0;
	}

	memcpy(kp->pubk, p, pklen);

	EVP_PKEY_assign(pkey, NID_alg, kp);
	return 1;
}

static int easyengine_security_bits(const EVP_PKEY *pkey) {
	// TODO: figure this out
	return 256;
}

static int easyengine_pubkey_size(const EVP_PKEY *pkey) {
	return *pkey_pk_size;
}

static int easyengine_pubkey_bits(const EVP_PKEY *pkey) {
	return *pkey_pk_size * 8;
}

int easyengine_register_ameths(EVP_PKEY_ASN1_METHOD **ameth, const char *pem_str, const char *info) {
	debug("REGISTER AMETH NID(%d/%s):%s:%s\n",NID_alg,OBJ_nid2sn(NID_alg),pem_str,info);

	*ameth = EVP_PKEY_asn1_new(NID_alg, EASYENGINE_EVP_PKEY_ASN1_FLAGS, pem_str, info);
	if (!*ameth)
		return 0;

	EVP_PKEY_asn1_set_public(*ameth, easyengine_pub_decode, easyengine_pub_encode, easyengine_pub_cmp, easyengine_pub_print, easyengine_pubkey_size, easyengine_pubkey_bits);
	EVP_PKEY_asn1_set_private(*ameth, easyengine_priv_decode, easyengine_priv_encode, easyengine_priv_print);
	EVP_PKEY_asn1_set_ctrl(*ameth, easyengine_ctrl);

	EVP_PKEY_asn1_set_param(*ameth, 0, 0, 0, 0, easyengine_cmp_parameters, 0);
	EVP_PKEY_asn1_set_security_bits(*ameth, easyengine_security_bits);
	EVP_PKEY_asn1_set_free(*ameth, easyengine_free);

	return 1;
}

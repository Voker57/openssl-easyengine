#pragma once

#include <openssl/evp.h>

int easyengine_register_ameths(EVP_PKEY_ASN1_METHOD **ameth, const char *pem_str, const char *info);

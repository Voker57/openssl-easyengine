#include <string.h>

#include <openssl/rand.h>

#include "easyengine_meth.h"
#include "easyengine.h"
#include "ossl_aes.h"
#include "easyengine.h"
#include "api_vars.h"
#include "debug/debug.h"
#include "easyengine_keypair.h"

#define TAG_SIZE 7
#define IV_SIZE 16

static int easyengine_keygen(EVP_PKEY_CTX *ctx, EVP_PKEY *pkey) {
	EASYENGINE_KEYPAIR *kp = easyengine_keypair_new(0);

	if(kp == NULL)
		return -1;

	pkey_generate_keypair(kp->pubk, kp->privk);
	EVP_PKEY_assign(pkey, NID_alg, kp);

	return 1;
}


static int easyengine_ctrl(EVP_PKEY_CTX *ctx, int type, int p1, void *p2) {
	int md_nid;
	const char *type_str = "";
	switch(type) {
	default:
		warn("UNSUPPORTED operation (type=%d).\n", type);
		return -2;
	}
	verbose("STUB (type=%s).\n", type_str);
	return 1;
}



static int easyengine_kem_encrypt(EVP_PKEY_CTX *ctx, unsigned char *out, size_t *outlen, const unsigned char *in,                                              size_t inlen) {
	int ct_size =
		+ *kem_encrypted_key_size
		+ TAG_SIZE
		+ IV_SIZE
		+ inlen;
	if(out == NULL) {
		// This is to probe for size
		*outlen = ct_size;
		return 1;
	}

	unsigned char *aeskey = OPENSSL_secure_malloc(32);
	unsigned char *tag = out + *kem_encrypted_key_size;
	unsigned char *iv = out + *kem_encrypted_key_size + TAG_SIZE;

	EVP_PKEY *pubk = EVP_PKEY_CTX_get0_pkey(ctx);
	if(pubk == NULL) {
		errorf("pubk is NULL\n");
		goto err;
	}
	EASYENGINE_KEYPAIR *r_keys = EVP_PKEY_get0(pubk);
	if(r_keys == NULL) {
		errorf("r_keys is NULL\n");
		goto err;
	}

	if(*outlen < ct_size) {
		errorf("outlen too small: %d/%d\n", *outlen, ct_size);
		goto err;
	}
	*outlen = ct_size;

	int r = RAND_bytes(aeskey, 32);
	if(r < 0) {
		errorf("RAND_bytes failed\n");
		goto err;
	}

	r = RAND_bytes(iv, 16);
	if(r < 0) {
		errorf("RAND_bytes failed 2\n");
		goto err;
	}

	r = kem_encapsulate(out, aeskey, r_keys->pubk);

	if(r != 1) {
		errorf("key encapsulation failed\n");
		goto err;
	}

	r = aes_256_gcm_encrypt(in, inlen, "", 0, aeskey, iv, IV_SIZE, out+*kem_encrypted_key_size+TAG_SIZE+IV_SIZE, tag, TAG_SIZE);
	if(r < 0) {
		errorf("AES encryption failed\n");
		goto err;
	}

	OPENSSL_secure_free(aeskey);
	return 1;

err:
	OPENSSL_secure_free(aeskey);

	return -1;
}

static int easyengine_kem_decrypt(EVP_PKEY_CTX *ctx, unsigned char *out, size_t *outlen, const unsigned char *in,                                              size_t inlen) {
	int pt_size = inlen
		- *kem_encrypted_key_size
		- IV_SIZE
		- TAG_SIZE;
	if(out == NULL) {
		// This is to probe for size
		*outlen = pt_size;
		return 1;
	}

	EVP_PKEY *pubk = EVP_PKEY_CTX_get0_pkey(ctx);
	if(pubk == NULL) {
		errorf("pubk is NULL\n");
		goto err;
	}
	EASYENGINE_KEYPAIR *r_keys = EVP_PKEY_get0(pubk);
	if(r_keys == NULL) {
		errorf("r_keys is NULL\n");
		goto err;
	}

	const unsigned char *tag = in + *kem_encrypted_key_size;
	const unsigned char *iv = in + *kem_encrypted_key_size + TAG_SIZE;

	unsigned char *aeskey = OPENSSL_secure_malloc(32);

	if(*outlen < pt_size) {
		errorf("outlen too small: %d/%d\n", *outlen, pt_size);
		goto err;
	}
	*outlen = pt_size;

	int r = kem_decapsulate(aeskey, in, r_keys->privk);

	if(r != 1) {
		errorf("key decapsulation failed\n");
		goto err;
	}

	r = aes_256_gcm_decrypt(in+ *kem_encrypted_key_size+TAG_SIZE+IV_SIZE,
			inlen-(*kem_encrypted_key_size+TAG_SIZE+IV_SIZE),
			"", 0, tag, TAG_SIZE, aeskey, iv, IV_SIZE, out);

	if(r < 0) {
		goto err;
	}

	OPENSSL_secure_free(aeskey);
	return 1;

err:
	OPENSSL_secure_free(aeskey);

	return -1;
}

static int easyengine_encrypt(EVP_PKEY_CTX *ctx, unsigned char *out, size_t *outlen, const unsigned char *in,                                              size_t inlen) {
	size_t ct_size =
		*pkey_encryption_overhead
		+ inlen;
	if(out == NULL) {
		// This is to probe for size
		*outlen = ct_size;
		return 1;
	}

	EVP_PKEY *pubk = EVP_PKEY_CTX_get0_pkey(ctx);
	if(pubk == NULL) {
		errorf("pubk is NULL\n");
		return 0;
	}
	EASYENGINE_KEYPAIR *r_keys = EVP_PKEY_get0(pubk);
	if(r_keys == NULL) {
		errorf("r_keys is NULL\n");
		return 0;
	}

	if(*outlen < ct_size) {
		errorf("outlen too small: %d/%d\n", *outlen, ct_size);
		return 0;
	}

	if(pkey_encrypt(out, outlen, in, inlen, r_keys->pubk) != 1) {
		errorf("encryption failed\n");
		return 0;
	}

	return 1;
}

static int easyengine_decrypt(EVP_PKEY_CTX *ctx, unsigned char *out, size_t *outlen, const unsigned char *in,                                              size_t inlen) {
	size_t pt_size = inlen;
	if(out == NULL) {
		// This is to probe for size
		*outlen = pt_size;
		return 1;
	}

	EVP_PKEY *pubk = EVP_PKEY_CTX_get0_pkey(ctx);
	if(pubk == NULL) {
		errorf("pubk is NULL\n");
		return 0;
	}
	EASYENGINE_KEYPAIR *r_keys = EVP_PKEY_get0(pubk);
	if(r_keys == NULL) {
		errorf("r_keys is NULL\n");
		return 0;
	}

	if(*outlen < pt_size) {
		errorf("outlen too small: %d/%d\n", *outlen, pt_size);
		return 0;
	}

	unsigned long long mlen = pt_size;
	if(pkey_decrypt(out, outlen, in, inlen, r_keys->privk) != 1) {
		errorf("decryption failed\n");
		return 0;
	}

	return 1;
}

static int easyengine_sign(EVP_PKEY_CTX *ctx, unsigned char *sig,
	size_t *siglen, const unsigned char *tbs,
	size_t tbslen) {

	size_t siglen_needed = pkey_signature_size(tbslen);
	if(sig == NULL) {
		warn("min: %u\n", siglen_needed);
		*siglen = siglen_needed;
		return 1;
	}
	if(*siglen < siglen_needed) {
		errorf("siglen too small: %u/%u\n", *siglen, siglen_needed);
		return -1;
	}

	const EVP_PKEY *_pkey = NULL;
	const EASYENGINE_KEYPAIR *kpair = NULL;

	_pkey = EVP_PKEY_CTX_get0_pkey(ctx);
	if (_pkey == NULL) {
		errorf("PKEY is NULL\n");
		return -1;
	}
	kpair = EVP_PKEY_get0(_pkey);
	if (easyengine_keypair_is_invalid(kpair)) {
		errorf("Keypair is invalid\n");
		return -1;
	}
	if (kpair->has_private != 1) {
		errorf("Private key is missing\n");
		return -1;
	}

	if(pkey_sign(sig, siglen, tbs, tbslen, kpair->privk) != 1) {
		errorf("Signing failed\n");
		return 0;
	}

	return 1;
}

static int easyengine_verify(EVP_PKEY_CTX *ctx, const unsigned char *sig,
	size_t siglen, const unsigned char *tbv,
	size_t tbvlen) {
	const EVP_PKEY *_pkey = NULL;
	const EASYENGINE_KEYPAIR *kpair = NULL;

	_pkey = EVP_PKEY_CTX_get0_pkey(ctx);
	if (_pkey == NULL) {
		errorf("PKEY is NULL\n");
		return -1;
	}
	kpair = EVP_PKEY_get0(_pkey);
	if (easyengine_keypair_is_invalid(kpair)) {
		errorf("keypair is invalid\n");
		return -1;
	}

	if (sig == NULL) {
		errorf("Bad signature\n");
		return -1;
	}

	if (pkey_verify(sig, siglen, tbv, tbvlen, kpair->pubk) == 1) {
		return 1;
	} else {
		errorf("Verification failed\n");
		return 0;
	}
}


int easyengine_register_pmeths(EVP_PKEY_METHOD *pmeth) {
	EVP_PKEY_meth_set_keygen(pmeth, NULL, easyengine_keygen);
	EVP_PKEY_meth_set_ctrl(pmeth, easyengine_ctrl, NULL); // TODO: easyengine_ctrl_str

	if(engine_implements_kem()) {
		EVP_PKEY_meth_set_encrypt(pmeth, NULL, easyengine_kem_encrypt);

		EVP_PKEY_meth_set_decrypt(pmeth, NULL, easyengine_kem_decrypt);
	} else if (engine_implements_pkey_encryption()) {
		EVP_PKEY_meth_set_encrypt(pmeth, NULL, easyengine_encrypt);

		EVP_PKEY_meth_set_decrypt(pmeth, NULL, easyengine_decrypt);
	}

	if(engine_implements_pkey_signatures()) {
		EVP_PKEY_meth_set_sign(pmeth, NULL, easyengine_sign);
		EVP_PKEY_meth_set_verify(pmeth, NULL, easyengine_verify);
	}

	return 1;
}

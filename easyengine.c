#include <openssl/objects.h>
#include <stdio.h>
#include <stdlib.h>
#include <openssl/engine.h>
#include "meths/easyengine_meth.h"
#include "meths/easyengine_asn1_meth.h"
// #include "objects.h"
#include "debug/debug.h"
#include <dlfcn.h>
#include "api_vars.h"
#include <stdbool.h>
#include "easyengine.h"

#define sizeof_static_array(a) \
	( (sizeof((a))) / sizeof((a)[0]) )



#define EASYENGINE_DEBUG_DEFAULT_LEVEL LOG_WARN
#define EASYENGINE_DEBUG_ENVVAR "EASYENGINE_DEBUG"

static EVP_PKEY_METHOD *pmeth_easyengine = NULL;
static EVP_PKEY_ASN1_METHOD *ameth_easyengine = NULL;


int NID_alg;
static void* dl_h = 0;

static int easyengine_pkey_meth_nids[] = {
	0,
	0
};

static int easyengine_ameth_nids[] = {
	0,
	0
};

static void easyengine_pkey_meth_nids_init() {
	easyengine_pkey_meth_nids[0] = NID_alg;
}

static void easyengine_ameth_nids_init() {
	easyengine_ameth_nids[0] = NID_alg;
}

static int easyengine_e_init(ENGINE *e) {
	return 1;
}

static int easyengine_e_destroy(ENGINE *e) {
	debug_logging_finish();
	OBJ_cleanup();
	if(dl_h) {
		dlclose(dl_h);
	} // TODO: catch errors
	return 1;
}

static int easyengine_e_finish(ENGINE *e) {
	return 1;
}


int register_nids() {
	char *oid_str = *alg_oid;
	char *sn = *alg_name;
	char *ln = *alg_name; // TODO: human name
	int new_nid = NID_undef;

	if (NID_undef != (new_nid = OBJ_sn2nid(sn)) ) {
		debug("'%s' is already registered with NID %d\n", sn, new_nid);
		return new_nid;
	}

	new_nid = OBJ_create(oid_str, sn, ln);

	if (new_nid == NID_undef) {
		fatalf("Failed to register NID for '%s'\n", ln);
		return 0;
	}
	debug("Registered '%s' with NID %d\n", sn, new_nid);

	ASN1_OBJECT *obj = OBJ_nid2obj(new_nid);
	if ( !obj ) {
		fatalf("Failed to retrieve ASN1_OBJECT for dinamically registered NID\n");
		return 0;
	}

	if ( NID_undef == new_nid ) {
		errorf("Failed to register NID for '%s'\n", sn );
		return 0;
	}


	NID_alg = new_nid;

	return 1;
}

bool engine_implements_pkey_encryption() {
	return pkey_encrypt != NULL;
}

bool engine_implements_kem() {
	return kem_encapsulate != NULL && *kem_secret_size >= 32; // TODO: be a bit more flexible here?
}

bool engine_implements_pkey_signatures() {
	return pkey_sign != NULL;
}

static int easyengine_register_pmeth(EVP_PKEY_METHOD **pmeth, int flags) {
	*pmeth = EVP_PKEY_meth_new(NID_alg, flags);

	if (*pmeth == NULL)
		return 0;

	return easyengine_register_pmeths(*pmeth);
}

static int easyengine_register_ameth(EVP_PKEY_ASN1_METHOD **ameth, int flags) {
	const char *pem_str = NULL;
	const char *info = NULL;

	if (!ameth)
		return 0;


	if (!OBJ_add_sigid(NID_dsa, NID_sha512, NID_alg)) {
		errorf("OBJ_add_sigid() failed\n");
		return 0;
	}

	pem_str = OBJ_nid2sn(NID_alg);

	info = OBJ_nid2ln(NID_alg);
	return easyengine_register_ameths(ameth, pem_str, info);
}

static int easyengine_register_methods() {
	return easyengine_register_pmeth(&pmeth_easyengine, 0) &&
		easyengine_register_ameth(&ameth_easyengine, 0);
}

static int easyengine_pkey_meths(ENGINE *e, EVP_PKEY_METHOD **pmeth, const int **nids, int nid) {
	if(!pmeth) {
		*nids = easyengine_pkey_meth_nids;
		return sizeof_static_array(easyengine_pkey_meth_nids) - 1;
	}
	if (nid == NID_alg) {
		*pmeth = pmeth_easyengine;
		return 1;
	}

	debug("requested nid not found\n");
	*pmeth = NULL;
	return 0;
}

static int easyengine_ameths(ENGINE *e, EVP_PKEY_ASN1_METHOD **ameth, const int **nids, int nid) {
	if(!ameth) {
		*nids = easyengine_ameth_nids;
		return sizeof_static_array(easyengine_ameth_nids) - 1;
	}

	if (nid == NID_alg) {
		*ameth = ameth_easyengine;
		return 1;
	}

	errorf("requested nid not found\n");
	*ameth = NULL;
	return 0;
}

#define load_symbol(SN, OPTIONAL) \
	(SN) = (typeof(SN))dlsym(dl_h, #SN); \
	err = dlerror(); \
	if(err != NULL) { \
		if (OPTIONAL) {\
			debug("error loading symbol %s: %s\n", #SN, err); \
			load_failed = true; \
		} else { \
			errorf("error loading symbol %s: %s\n", #SN, err); \
			goto l_err; \
		} \
	} else { \
		load_failed = false; \
	}

static int easyengine_load_api() {
	char *engine_so = getenv("EASYENGINE_ENGINE"); // TODO: maybe use openssl commands to load it?
	if(engine_so == NULL) {
		errorf("$EASYENGINE_ENGINE not set\n");
		return 0;
	}

	char *err;
	void *dl_h;
	bool load_failed;

	dl_h = dlopen(engine_so, RTLD_NOW);
	if(dl_h == NULL) {
		errorf("loading engine failed: %s\n", dlerror());
		return 0;
	}

	load_symbol(engine_id, false);
	load_symbol(alg_name, false);
	load_symbol(alg_oid, false);
	load_symbol(pkey_sk_size, false);
	load_symbol(pkey_pk_size, false);
	load_symbol(pkey_generate_keypair, false);

	load_symbol(pkey_signature_size, true);
	if (!load_failed) {
		load_symbol(pkey_sign, false);
		load_symbol(pkey_verify, false);
	} else {
		pkey_sign = NULL;
		pkey_verify = NULL;
	}

	load_symbol(pkey_encryption_overhead, true);
	if (!load_failed) {
		load_symbol(pkey_encrypt, false);
		load_symbol(pkey_decrypt, false);
	} else {
		pkey_encrypt = NULL;
		pkey_decrypt = NULL;
	}

	load_symbol(kem_encrypted_key_size, true);
	if (!load_failed) {
		load_symbol(kem_secret_size, false);
		load_symbol(kem_encapsulate, false);
		load_symbol(kem_decapsulate, false);
	} else {
		kem_secret_size = NULL;
		kem_encapsulate = NULL;
		kem_decapsulate = NULL;
	}

	return 1;
l_err:
	if(!dlclose(dl_h)) {
		errorf("dlclose failed! %s", dlerror());
	}
	return 0;

}

int easyengine_bind(ENGINE *e, const char *id) {
	debug_logging_init(EASYENGINE_DEBUG_DEFAULT_LEVEL, EASYENGINE_DEBUG_ENVVAR);


	int ret = 0;

	if (!easyengine_load_api()) {
		errorf("easyengine_load_api failed\n");
		goto end;
	}

	if (!ENGINE_set_id(e, *engine_id)) {
		errorf("ENGINE_set_id failed\n");
		goto end;
	}
	if (!ENGINE_set_name(e, *engine_id)) { // TODO: different name?
		errorf("ENGINE_set_name failed\n");
		goto end;
	}

	if(!ENGINE_set_init_function(e, easyengine_e_init)) {
		errorf("ENGINE_set_init_function failed\n");
		goto end;
	}
	if(!ENGINE_set_destroy_function(e, easyengine_e_destroy)) {
		errorf("ENGINE_set_destroy_function failed\n");
		goto end;
	}
	if(!ENGINE_set_finish_function(e, easyengine_e_finish)) {
		errorf("ENGINE_set_finish_function failed\n");
		goto end;
	}

	if (!register_nids()) {
		errorf("Failure registering NIDs\n");
		goto end;
	}

	easyengine_pkey_meth_nids_init();
	easyengine_ameth_nids_init();

	if (!easyengine_register_methods()) {
		errorf("Failure registering methods\n");
		goto end;
	}

	if (!ENGINE_set_pkey_meths(e, easyengine_pkey_meths)) {
		errorf("ENGINE_set_pkey_meths failed\n");
		goto end;
	}

	if (!ENGINE_set_pkey_asn1_meths(e, easyengine_ameths)) {
		errorf("ENGINE_set_pkey_meths failed\n");
		goto end;
	}

	ret = 1;
end:
	return ret;
}

#pragma GCC visibility push(default)
IMPLEMENT_DYNAMIC_BIND_FN(easyengine_bind)
IMPLEMENT_DYNAMIC_CHECK_FN()
#pragma GCC visibility pop

#pragma once
#define EXPORT __attribute__ ((visibility("hidden")))

#include <stdbool.h>

extern int NID_alg;
bool engine_implements_pkey_encryption();

bool engine_implements_kem();

bool engine_implements_pkey_signatures();

EXPORT int easyengine_bind(ENGINE *e, const char *id);

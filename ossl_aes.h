#pragma once

int aes_256_gcm_encrypt(const unsigned char *plaintext, int plaintext_len, const unsigned char *aad,
	int aad_len, unsigned char *key, const unsigned char *iv, int iv_len,
	unsigned char *ciphertext, unsigned char *tag, unsigned int tagsize);

int aes_256_gcm_decrypt(const unsigned char *ciphertext, int ciphertext_len, const unsigned char *aad,
	int aad_len, const unsigned char *tag, unsigned int tagsize, const unsigned char *key, const unsigned char *iv,
	int iv_len, unsigned char *plaintext);
